# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   main.py                                                                  *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

import os
import time
import random
import string

from scapy.all import *
from data_management import *
from env import *
	
def get_random_string(length, old_random_string):
	letters = string.ascii_lowercase
	result_str = ''.join(random.choice(letters) for i in range(length))
	if (old_random_string == result_str):
		return (get_random_string(length))
	old_random_string = result_str
	return (result_str)

def send_data(id, data):
	global sender_mac
	global iface
	ssid = "*" + id + ";" + data + "*"
	dot11 = Dot11(type=0, subtype=8, addr1="ff:ff:ff:ff:ff:ff", addr2=sender_mac, addr3=sender_mac)
	beacon = Dot11Beacon()
	essid = Dot11Elt(ID="SSID", info=ssid, len=len(ssid))
	frame = RadioTap()/dot11/beacon/essid
	sendp(frame, inter=0.1, iface=iface, loop=0)

def submission_module():
	global old_random_string
	global size_data_read
	global number_of_sending
	with os.scandir(base_path_data_encode) as entries:
		for entry in entries:
			if entry.is_file():
				f = open(base_path_data_encode + entry.name, 'r')
				random_string = get_random_string(2, old_random_string)
				old_random_string = random_string
				repeat_submission = 0
				data = f.read(size_data_read).strip()
				while (data != ""):
					while (repeat_submission < number_of_sending):
						send_data(random_string, data.strip())
						repeat_submission = repeat_submission + 1
						print(str(repeat_submission) + " - " + random_string + ":" + data.strip())
					data = f.read(size_data_read).strip()
					repeat_submission = 0
					time.sleep(15)
				time.sleep(10)
				f.close()

if __name__ == "__main__":
	data_management_encoding()
	submission_module()
