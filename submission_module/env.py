# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   env.py                                                                   *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

# Setting
base_path_data = "send_data/"
base_path_data_encode =  base_path_data + "encoding_data/"
old_random_string = "42"
size_data_read = 18
number_of_sending = 100

# Data interface submission
sender_mac = "a8:20:77:74:26:66"
iface = "wlan0"
