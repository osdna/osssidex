# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   encoding_data.py                                                         *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

import base64

def encoding_data_base64(data_bytes):
	base64_bytes = base64.b64encode(data_bytes)
	base64_data = base64_bytes.decode('ascii')
	return (base64_bytes)
