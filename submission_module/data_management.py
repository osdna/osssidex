# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   data_management.py                                                       *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

import os

from encoding_data import *
from env import *

def data_management_encoding():
	with os.scandir(base_path_data) as entries:
		for entry in entries:
			if entry.is_file():
				f_r = open(base_path_data + entry.name, 'rb')
				f_w = open(base_path_data_encode + entry.name, 'wb')
				f_w.write(encoding_data_base64((f_r.read())))
				f_r.close()
				f_w.close()

def read_data():
	list_data = []
	with os.scandir(base_path_data_encode) as entries:
		for entry in entries:
			if entry.is_file():
				f = open(base_path_data_encode + entry.name, 'r')
				data = 1
				while (data):
					data = f.read(27)
					if (data != ""):
						list_data.append(data)
				f.close()
	return (list_data)

if __name__ == "__main__":
	data_management_encoding()
