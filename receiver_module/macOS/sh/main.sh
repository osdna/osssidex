# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   main.sh                                                                  *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

init_key=""
init_data=""
framework="/System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport -s"

while [ 1 ]
do
	tmp_data=$($framework | grep a8:20:77:74:26:66 | cut -d"*" -f2)
	key=$(echo $tmp_data | cut -d";" -f1)
	data=$(echo $tmp_data | cut -d";" -f2)
	echo "key : $key"
	echo "data : $data"
	echo "init_key : $init_key"
	echo "init_data : $init_data"
	if [ "$key" != "$init_key" ]
	then
		init_key=$key
		init_data="-42"
	fi
	if [ "$key" == "$init_key" ]
	then
		if [ "$init_data" != "$data" ]
		then
			init_data=$data
			$(echo "echo -n $data >> $key.txt" | bash)
		fi
	fi
done
