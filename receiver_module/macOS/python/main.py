# * ************************************************************************** *
# *                                                                            *
# *                                                                            *
# *   main.py                                                                  *
# *                                                                            *
# *   By: osxcode <@osxcode>                                                   *
# *                                                                            *
# *   Created: 2021/07/12 00:42:42 by osxcode                                  *
# *   Updated: 2021/07/12 00:42:42 by osxcode                                  *
# *                                                                            *
# * ************************************************************************** *

import subprocess
import time

init_key = "-42"
init_data = "-42"
framework = "/System/Library/PrivateFrameworks/Apple80211.framework/Resources/airport -s"

while (True):
	op = subprocess.check_output(framework + " | grep a8:20:77:74:26:66 | cut -d'*' -f2", shell = True)
	try:
		key, data = op.decode("utf-8") .split(";")
		print("key : " + key)
		print("data : " + data)
		print("init_key : " + init_key)
		print("init_data : " + init_data)
		if (key != init_key):
			init_key = key
			init_data = "-42"
		if ((key == init_key) and (init_data.strip() != data.strip())):
			init_data = data.strip()
			data_tmp = "echo -n '" + data.strip() + "' >> " + key + ".txt"
			subprocess.check_output('echo "' + data_tmp + '" | bash' , shell = True)
	except:
		pass
