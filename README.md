# osssidex

The osssidex module is a python program allowing data exfiltration using the [SSID](https://en.wikipedia.org/wiki/Service_set_(802.11_network)) / [BSSID](https://en.wiktionary.org/wiki/BSSID) attributes of the wi-fi data exchange protocol according to [IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11) standard.<br>
The module is broken down into two subunits, namely the exfiltration and the reception one.


```diff
The module specifically targets the [macOS] platform.
```




## Working

To allow data exfiltration, the osssidex module uses the operation of the [IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11) standard.<br>
Among other things, the standard describes the structure and operation of the connection from a terminal to an access point.<br>
To operate, the module uses the information contained in the frames including in particular the SSID / BSSID sections.
<br />
<br />

The osssidex module diverts the function of the previous sections as follows :

* The information contained in the BSSID section as a selection identifier
* The information contained in the SSID as the payload data

This method of exfiltration is nevertheless limited by :

* The size of the sections of the [IEEE 802.11](https://en.wikipedia.org/wiki/IEEE_802.11) standard
* By the minimum transmission time of packets for detection by the airport binary
* Potential loss of information as our operating model relates more to UDP.

Many avenues for improvement are possible and more detailed in the Further development section.




## Requirements

The osssidex module uses some dependencies to be installed beforehand in order to ensure that it works properly.
<br>

The list of necessary dependencies :
<br>
<br>

-- submission_module --

* [scapy](https://scapy.readthedocs.io/en/latest/installation.html) : Scapy is a Python program that enables the user to send, sniff and dissect and forge network packets. 

* [aircrack-ng](https://www.aircrack-ng.org/index.html) : Aircrack-ng is a complete suite of tools to assess WiFi network security.
<br>

-- receiver_module --

* [Airport](https://ss64.com/osx/airport.html) : Airport manages 802.11 interfaces.

* [Wireless network interface controller](https://en.wikipedia.org/wiki/Wireless_network_interface_controller) : Stand-alone electronic card designed to fulfill this role of network interface.<br>
Required for both sending and receiving system.<br><br>

The airport binary is already present on the macOS operating system of the user who will receive the information.<br>
Otherwise, it is necessary to recover the binary beforehand.




## Standard installation

To install the latest development version, the following procedure is necessary :

```bash
# 1. Download the latest version of the module.

# 2. Unzip the obtained module.

# 3. Install the Scapy dependency on the sending system :

	-- pip install scapy

# 4. Check that the aircrack-ng binary is present on the sending system.

# 5. Configure the env.py file in the submission_module folder with the appropriate information.

	# Setting
	* base_path_data : Folder containing the data to be sent.
	* base_path_data_encode : File containing the encoded data preprocessing before sending.
	* old_random_string : Initial random identifiers.
	* size_data_read : Size of data read for sending.
	* number_of_sending : Number of sending of the same information.

	# Data interface submission
	* sender_mac = BSSID from the sending system.
	* iface = Interface name of the sending system.


# 6. Position the data to be transferred in the folder specified in the env.py configuration file.

# 7. Switch the wi-fi card of the submission machine to monitor mode.

	-- sudo airmon-ng check
	-- sudo airmon-ng check kill
	-- sudo airmon-ng start wlan0
	-- sudo iwconfig

# 8. Copy the files from the receiving module in the data_receiver folder into the receiving machine.


Latest version : https://gitlab.com/osxcode/osssidex
```




## Use

The program can be started using the main.py file in the respective directories of the send and receive modules :

```diff
Running the send module requires root rights.
```

```bash
# Send module
sudo python3 main.py

# Receive module
python3 main.py
```




## Data structure

Here is an example of data structure sent by the submission module.


```data
"*" + id + ";" + data + "*"
```

- id : The key id corresponds to a random identifier generated for the transfer of specific data.
- data : The key data corresponds to a portion of the give send.




## Tests



#### Hardware / Software


Information - The tests as well as the development of the module have been carried out with the versions of the following tools and systems :
 

- Python 3.9.6

- AirPort 13.0

- Target system for sending data : Kali Linux 2021.2
 
- Target system for data reception : macOS 10.15.7 (19H114)

  


#### Result example 


```text
# Send module
sudo python main.py
.
Sent 1 packets.
1 - uu:I2luY2x1ZGUgPHN0ZG
.
Sent 1 packets.
2 - uu:I2luY2x1ZGUgPHN0ZG
.

***

Sent 1 packets.
100 - uu:I2luY2x1ZGUgPHN0ZG



# Receive module
python main.py
key : uu
data : I2luY2x1ZGUgPHN0ZG

init_key : -42
init_data : -42
key : uu
data : I2luY2x1ZGUgPHN0ZG

init_key : uu
init_data : I2luY2x1ZGUgPHN0ZG
key : uu
data : I2luY2x1ZGUgPHN0ZG
```




## Maintainers

Current maintainer :

* osxcode - [https://gitlab.com/osxcode/osssidex](https://gitlab.com/osxcode/osssidex)




## Further development

- Improved transmission algorithm.
- Optimization of tram identifiers.
- Tram integrity verification mechanism.
- Encryption of tram information.
- Optimization of the reception module.
- Development of reception modules for other platforms.




## Privacy

```diff
The module does not collect or store any personal data about the user.
```



## Warning

It is the end user's responsibility to obey all applicable local, state and federal laws.

The author is in no way responsible for any misuse or damage caused by this program.




## Support

If you want you can support me ;)

[-- OXEN --](https://oxen.io)


<img src="https://osxcode.gitlab.io/os/img/QRCODE_OXEN_WALLET.png"  width="500" height="350">




## Licence  

[LGPL - Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html)
